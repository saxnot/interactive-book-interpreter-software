# Interactive Book Interpreter-Software

<a href="url"><img src="http://i.imgur.com/Grjkm3P.png" height="160"></a>

IBIS can be used for displaying interactive books (sometimes called [Gamebooks](https://en.wikipedia.org/wiki/Gamebook)).
  

## Using IBIS

IBIS currently only displays a small example story which was created for debugging purposes.  
The file can be rewritten and the software recompiled to use your own story.

Maybe I add command line arguments or similar later.


## Contributing

It's just a super-small side project to kill some time and have fun with my siblings. The software is pretty much finished.

This software was written for a small book-project of my siblings.  
Features will be added as necessary for their book.


##  How is IBIS licensed?

It's licensed under the MIT License.  
See "LICENSE"-File or www.opensource.org/licenses/MIT for more informations.


## Author

I'm the Author (Jonas Dralle)  

You can contact me via the E-Mail address listed on my GitHub profile (may require log in).