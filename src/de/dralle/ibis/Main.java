package de.dralle.ibis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Code Entry Point for the Text Story Engine.
 * <p>
 * This software allows the displays of interactive fiction like an interactive book.
 * </p>
 *
 * @author Jonas Dralle, 06.07.2017
 */
public class Main {

    public static void main(String[] args) throws IOException {

        StringBuilder storyString = readStoryStringFromStoryTxt();
        Story story = StoryParser.readStoryFromString(storyString.toString());
        new SwingStoryRunner(story).run();
    }

    private static StringBuilder readStoryStringFromStoryTxt() throws IOException {
        try (FileReader fileReader = new FileReader("example-story.txt");
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            StringBuilder storyString = new StringBuilder();
            bufferedReader.lines().forEach(l -> {
                storyString.append(l);
                storyString.append("\n");
            });
            return storyString;
        }
    }
}
