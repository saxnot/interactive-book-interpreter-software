package de.dralle.ibis;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * An interactive story
 * <p>
 * This object stores various {@link StoryNode}s which the reader can visit.
 * </p>
 *
 * @author Jonas Dralle, 06.07.2017
 */
public class Story {
    private List<StoryNode> nodes = new ArrayList<>();

    private StoryNode currentNode;

    public Story(List<StoryNode> nodes) {
        Objects.requireNonNull(nodes, "Nodes may not be null");
        if (nodes.size() == 0) {
            throw new IllegalArgumentException("There needs to be at least one node.");
        }

        // First Element in List = Start of Story
        currentNode = nodes.get(0);

        this.nodes = nodes;
    }

    public StoryNode getCurrentNode() {
        return currentNode;
    }

    public void nextStorySelection(StoryChoice nextStoryChoice) {
        currentNode = nextStoryChoice.getNextNode();
    }
}
