package de.dralle.ibis;

import javax.swing.*;

/**
 * @author Jonas Dralle, 08.07.2017
 */
public class SwingStoryRunner extends StoryRunner {

    private JPanel panel;

    public SwingStoryRunner(Story story) {
        super(story);
        initGUI();
    }

    private void initGUI() {
        JFrame frame = new JFrame("IBIS - Interactive Book Interpreter-Software");
        frame.setSize(800, 600);

        panel = new JPanel();
        frame.add(panel);
        frame.setVisible(true);
    }

    @Override
    protected void displayNode(StoryNode node) {
        // clear panel
        panel.removeAll();

        displayText(node);

        for (StoryChoice storyChoice : node.getNextNodes()) {
            JButton choiceButton = new JButton(storyChoice.getText());
            choiceButton.addActionListener(click -> choiceSelected(storyChoice));
            panel.add(choiceButton);
        }
        // update panel
        panel.updateUI();
    }

    @Override
    protected void endOfStory(StoryNode node) {
        // clear panel
        panel.removeAll();

        displayText(node);

        panel.add(new JLabel("END OF STORY"));

        // update panel
        panel.updateUI();
    }

    private void displayText(StoryNode node) {
        JTextArea jTextArea = new JTextArea(node.getText());
        jTextArea.setLineWrap(true);
        panel.add(jTextArea);
    }
}
