package de.dralle.ibis;

import java.util.Scanner;

/**
 * Runs a story inside standard output.
 *
 * @author Jonas Dralle, 06.07.2017
 */
public class ConsoleStoryRunner extends StoryRunner {

    private static Scanner in = new Scanner(System.in);

    public ConsoleStoryRunner(Story story) {
        super(story);
    }

    @Override
    protected void displayNode(StoryNode node) {
        displayText(node);

        // display all choices
        for (StoryChoice storyChoice : node.getNextNodes()) {
            int index = 1 + node.getNextNodes().indexOf(storyChoice);
            System.out.println("(" + index + ") " + storyChoice.getText());
        }

        // read selected choice
        while (true) {
            int input = in.nextInt();
            if (input < 1
                    || input > node.getNextNodes().size()) {
                // Wrong Input
                System.out.println("Sorry I haven't understood that.");
                continue;
            }
            choiceSelected(node.getNextNodes().get(input - 1));
        }
    }

    @Override
    protected void endOfStory(StoryNode node) {
        displayText(node);
        System.out.println("\n~~ End of Story ~~");
    }

    private void displayText(StoryNode node) {
        System.out.println(node.getText());
        System.out.println();
        System.out.println();
    }
}
