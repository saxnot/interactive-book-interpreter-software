package de.dralle.ibis;

import java.util.ArrayList;
import java.util.List;

/**
 * A piece of content of an interactive story
 * <p>
 * A {@link Story} consists of multiples nodes. Every node has a text which wil be displayed and choices which the user can make.
 * If the node has no further choices then the story ends at this node.
 * </p>
 *
 * @author Jonas Dralle, 06.07.2017
 */
public class StoryNode {

    /**
     * An identifier which is used to refer to other StoryNodes before these are serialized.
     */
    private String internalId;

    /**
     * Text which is displayed in this story node
     */
    private String text;

    /**
     * Possible choices which the reader might choose from.
     */
    private List<StoryChoice> nextNodes;

    /**
     * New StoryNode
     *
     * @param internalId {@link #internalId}
     * @param text       {@link #text}
     */
    public StoryNode(String internalId, String text) {
        this.internalId = internalId;
        this.text = text;
    }

    /**
     * @return {@link #internalId}
     */
    public String getInternalId() {
        return internalId;
    }

    /**
     * @return {@link #text}
     */
    public String getText() {
        return text;
    }

    /**
     * Add a Choice to this StoryNode.
     *
     * @param storyChoice The StoryNode Object to be added
     * @return this instance (helpful for builder pattern)
     */
    public StoryNode addChoices(StoryChoice storyChoice) {
        if (nextNodes == null) {
            nextNodes = new ArrayList<>();
        }
        nextNodes.add(storyChoice);
        return this;
    }

    public List<StoryChoice> getNextNodes() {
        return nextNodes;
    }
}
