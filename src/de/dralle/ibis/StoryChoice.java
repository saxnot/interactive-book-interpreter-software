package de.dralle.ibis;

/**
 * Represents a choice inside a StoryNode
 * <p>
 * The reader can make choices while reading. These choices have a text which indicate what this choice does
 * i. e. "Drink mystery sirup" and also a StoryNode where the user will travel to when this action happens
 * (i. e. go to endscreen because te mystery sirup is deadly).
 * </p>
 *
 * @author Jonas Dralle, 06.07.2017
 */
public class StoryChoice {

    /**
     * The Text which is displayed to the reader
     */
    private String text;

    /**
     * The node the reader will travel to when this choice is selected
     */
    private StoryNode nextNode;

    /**
     * New StoryChoice
     *
     * @param text     {@link #text}
     * @param nextNode {@link #nextNode}
     */
    public StoryChoice(String text, StoryNode nextNode) {
        this.text = text;
        this.nextNode = nextNode;
    }

    /**
     * @return {@link #text}
     */
    public String getText() {
        return text;
    }

    /**
     * @param text {@link #text}
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return {@link #nextNode}
     */
    public StoryNode getNextNode() {
        return nextNode;
    }

    /**
     * @param nextNode {@link #nextNode}
     */
    public void setNextNode(StoryNode nextNode) {
        this.nextNode = nextNode;
    }
}
