package de.dralle.ibis;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Jonas Dralle, 06.07.2017
 */
public class StoryParser {

    static class StoryNodeConnection {
        String text;
        String nodeId;
    }

    public static Story readStoryFromString(String story) {
        Objects.requireNonNull(story, "String may not be null.");

        List<String> nodeStrings = Arrays.asList(story.split("[+]"));
        if (nodeStrings.size() == 0) {
            throw new InvalidParameterException("String may not be empty.");
        }

        List<StoryNode> nodes = new ArrayList<>();
        Map<StoryNode, List<StoryNodeConnection>> nodeChoices = new HashMap<>();

        for (int i = 0; i < nodeStrings.size(); i++) {
            List<String> singleNodeStrings = new ArrayList<>(Arrays.asList(nodeStrings.get(i).split("\n")));
            singleNodeStrings.removeIf(s -> s.startsWith("#"));

            // Empty nodes
            if (singleNodeStrings.size() < 2) {
                continue;
            }

            String internalId = singleNodeStrings.get(0);
            StringBuilder text = new StringBuilder();
            for (int line = 1; line < singleNodeStrings.size() && !singleNodeStrings.get(line).startsWith("-"); line++) {
                text.append(singleNodeStrings.get(line));
                text.append("\n");
            }

            // Create StoryNode
            StoryNode newNode = new StoryNode(
                    internalId,
                    text.toString());
            nodes.add(newNode);

            // Create Map of choices (because this room might refer to rooms which are not yet parsed)
            nodeChoices.put(newNode, new ArrayList<>());
            List<String> choiceTexts = singleNodeStrings
                    .stream()
                    .filter(s -> s.startsWith("-") && s.contains("::"))
                    .collect(Collectors.toList());
            for (String choiceText : choiceTexts) {
                choiceText = choiceText.substring(1);
                String[] roomConnection = choiceText.split("::");
                if (roomConnection.length != 2) {
                    System.out.println("[WARN] parsed story node Number " + i + " has invalid connection formatting \"" +
                            choiceText + "\". Will be ignored.");
                    continue;
                }
                StoryNodeConnection connection = new StoryNodeConnection();
                connection.text = roomConnection[0];
                connection.nodeId = roomConnection[1];
                nodeChoices.get(newNode).add(connection);
            }
        }

        // Create StoryChoice-Objects
        nodeChoices.forEach((storyNode, storyNodeConnections) -> {
            for (StoryNodeConnection storyNodeConnection : storyNodeConnections) {
                Optional<StoryNode> linkedNote = nodes
                        .stream()
                        .filter(a -> Objects.equals(a.getInternalId(), storyNodeConnection.nodeId))
                        .findFirst();
                if (!linkedNote.isPresent()) {
                    System.out.println("[WARN] parsed story node \"" + storyNode.getInternalId()
                            + "\" has connection with unknown id \"" + storyNodeConnection.nodeId + "\". Will be ignored.");
                    continue;
                }

                storyNode.addChoices(new StoryChoice(storyNodeConnection.text, linkedNote.get()));
            }
        });
        return new Story(nodes);
    }
}
