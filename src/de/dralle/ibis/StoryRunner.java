package de.dralle.ibis;

/**
 * Runs a story
 *
 * @author Jonas Dralle, 08.07.2017
 */
public abstract class StoryRunner {

    private Story story;

    public StoryRunner(Story story) {
        this.story = story;
    }

    public void run() {
        displayNode(story.getCurrentNode());
    }

    protected void choiceSelected(StoryChoice choice) {
        story.nextStorySelection(choice);

        // check for story End
        if (story.getCurrentNode().getNextNodes() == null
                || story.getCurrentNode().getNextNodes().size() == 0) {
            endOfStory(story.getCurrentNode());
            return;
        }

        // display next node
        displayNode(story.getCurrentNode());
    }

    /**
     * Displays a single StoryNode inside the standard output.
     *
     * @param node the StoryNode to display
     */
    protected abstract void displayNode(StoryNode node);

    protected abstract void endOfStory(StoryNode node);
}
